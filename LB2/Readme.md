# LB2

<!-- TOC -->

- [LB2](#lb2)
  - [Auftrag](#auftrag)
  - [Anforderungen](#anforderungen)
  - [Durchführung](#durchf%C3%BChrung)
    - [altes Deployment löschen und Service bearbeiten](#altes-deployment-l%C3%B6schen-und-service-bearbeiten)
    - [Deployment Template anpassen](#deployment-template-anpassen)
    - [Umgebung deployen und überpüfen](#umgebung-deployen-und-%C3%BCberp%C3%BCfen)
    - [Wie arbeiten die beiden Objekte Service und Deployment zusammen?](#wie-arbeiten-die-beiden-objekte-service-und-deployment-zusammen)
    - [Auf 10 Pods aufstocken](#auf-10-pods-aufstocken)
    - [Replica löschen](#replica-l%C3%B6schen)
    - [rolling update](#rolling-update)
    - [rollback](#rollback)

<!-- /TOC -->

## Auftrag

In diesem Auftrag wird eine Webapp mit 5 Pods (Container) Replicas und dem Image One erstellt. Dazu benötigt man ein Service und ein Deployment File. Danach sollten nochmals 5 Pods zusätzlich erstellt werden (ingesamt 10). Die Anwendung sollte dann so kunstruiert sein, dass wenn man einen Pod removed, dass sich die Anwendung automatisch selber repariert (self healing). Wenn das geklappt hat, sollte man einen rolling update durchführen, so dass 10 Replicas auf dem Image Two basierend laufen, ohne die Verfügbarkeit einzuschrenken. Wenn das auch geklappt hat wird ein Rollback durchgeführt, wo bei man wieder den alten Stand vo dem rolling update zurückgesetzt.

## Anforderungen

- 5 Pods
- Container Port: 8095
- Nodeport: 30200
- Label: brixel-mueller-app
- upgraden auf 10 Pods
- self-healing
- rolling update
- rollback

## Durchführung

### altes Deployment löschen und Service bearbeiten

1. Zuerst muss der alte Service und das alte Deployment removed werden

```bash
$kubectl delete svc mueller-svc
```

```bash
$kubectl delete deployment web-deploy
```

2. SVC Template anpassen mit **name, labelname und port**

```
## Autor: M. Mueller, D. Brixel
apiVersion: v1
kind: Service
metadata:
  name: brixel-mueller-svc
  labels:
    app: brixel-mueller-app
spec:
  type: NodePort
  ports:
  - port: 8095
    nodePort: 30200
    protocol: TCP
  selector:
    app: brixel-mueller-app
```

### Deployment Template anpassen

1. Deployment File anpassen mit **label, image, Container Port**

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: web-deploy
  labels:
    app: brixel-mueller-app
spec:
  replicas: 5
  selector:
    matchLabels:
      app: brixel-mueller-app
  template:
    metadata:
      labels:
        app: brixel-mueller-app
    spec:
      terminationGracePeriodSeconds: 1
      containers:
      - name: hello-pod
        image: registry.gitlab.com/mueller_max/m346/webapp-p8095_one:1.0
        ports:
        - containerPort: 8095
```

### Umgebung deployen und überpüfen

1. Beide .yml files deployen und überprüfen, ob es lauft

```bash
$kubectl apply -f brixel-mueller-deploy.yml
```

```bash
$kubectl apply -f brixel-mueller-svc.yml
```

2. mit get pods anzahl der Pods und auf 10.1.44.23:30200 überprüfen

```bash
$kubectl get pods
```

### Wie arbeiten die beiden Objekte (Service und Deployment) zusammen?

Der Service ist dafür verantwortlich, dass auf die Umgebung über den Nodeport zugegriffen werden kann. Über das labelmatching, weiss er welche Pods er für die Webapp benutzen kann, falls ein Request über Port 30200 rein kommt, wird der Port auf 8095 weitergeleitet und auf einen Pod zugegriffen, welcher den Request bearbeitet. Das Deployment file ist dafür den desired state einzuhalten und die Ports zu erstellen.

### Auf 10 Pods aufstocken

1. im Deployment File die Anzahl von replicas auf 10 Anpassen

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: web-deploy
  labels:
    app: brixel-mueller-app
spec:
  replicas: 10
  selector:
    matchLabels:
      app: brixel-mueller-app
  template:
    metadata:
      labels:
        app: brixel-mueller-app
    spec:
      terminationGracePeriodSeconds: 1
      containers:
      - name: hello-pod
        image: registry.gitlab.com/mueller_max/m346/webapp-p8095_one:1.0
        ports:
        - containerPort: 8095
```

2. Deployment removen und nochmals apply ausführen

```bash
$kubectl delete deployment brixel-mueller-deploy
```

```bash
$kubectl apply -f brixel-mueller-deploy.yml
```

### 1 Replica löschen

Pod Name auslesen

```bash
$kubectl get pods
```

```bash
$kubectl delete pod [Pod Name]
```

danach sollte ein neuer Pod erstellt werden, welchen den desired state wieder herstellt (self-healing)

### rolling update

1. neues file brixel-mueller-deploy-rolling-upgrade-complete.yml rolling update mit replicas, minReadySeconds, maxUnavailable, maxSurge

```
## Autor: M. Mueller, D. Brixel
apiVersion: apps/v1
kind: Deployment
metadata:
  name: web-deploy
  labels:
    app: brixel-mueller-app
spec:
  selector:
    matchLabels:
      app: brixel-mueller-app
  replicas: 10
  minReadySeconds: 5
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 0
      maxSurge: 1
  template:
    metadata:
      labels:
        app: brixel-mueller-app
    spec:
      terminationGracePeriodSeconds: 1
      containers:
      - name: brixel-mueller-container
        image: registry.gitlab.com/mueller_max/m346/webapp-p8095_two
        imagePullPolicy: Always
        ports:
        - containerPort: 8095
```

2. rolling update file deployen

```bash
$kubectl apply -f brixel-mueller-deploy-rolling-upgrade-complete.yml
```

3. mit watch prozess und auf website überprüfen

```bash
$kubectl get pods --watch
```

### rollback

```bash
$kubectl rollout history deploy web-deploy
```

```bash
$kubectl rollout undo deploy web-deploy --to-revision=1
```
