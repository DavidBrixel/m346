# Aufgabe 1.1

<!-- TOC -->

- [Aufgabe 1.1](#aufgabe-11)
  - [Anforderungen](#anforderungen)
  - [Webapp in die VM clonen](#webapp-in-die-vm-clonen)
  - [Port ändern](#port-%C3%A4ndern)
  - [Hintergrundfarbe ändern](#hintergrundfarbe-%C3%A4ndern)
  - [PNG ersetzen](#png-ersetzen)
  - [Image erstellen auf Gitlab pushen und runen](#image-erstellen-auf-gitlab-pushen-und-runen)
  - [Container stoppen und dann image löschen?](#container-stoppen-und-dann-image-l%C3%B6schen)
  - [Quellen](#quellen)

<!-- /TOC -->

## Anforderungen

- Port 8091
- Hintergrund Farbe des Textes in Goldgelb ändern
- altes png ersetzen
- run container von Image auf gitlab

## Webapp in die VM clonen

```
git clone https://gitlab.com/ser-cal/docker-bootstrap/-/tree/main/

mv ./first-container /home/ubuntu/M346
```

## Port ändern

```
nano app.js
```

Danach auf der Zeile 12 den Eintrag 8080 in 8091 umschreiben und speichern

## Hintergrundfarbe ändern

```
cd static
cd css
nano main.css
```

.content #message {

background-color: [ändern zu #ebd63d];

}

## PNG ersetzen

```
rm image.png

wget https://gitlab.com/ser-cal/m346-lab/-/raw/main
```

## Image erstellen auf Gitlab pushen und runen

```
docker login registry.gitlab.com -u david902

docker image build -t registry.gitlab.com/david902/m346/webapp_david902_8091:1.0 .

docker image push registry.gitlab.com/david902/m346/webapp_david902_8091:1.0

docker container run -d --name webapp_david902_8091 -p 8091:8091 registry.gitlab.com/david902/m346/webapp_david902_8091:1.0
```

## Container stoppen und dann image löschen?

Nein zuerst muss der Container gelöscht werden, stoppen reicht nicht

## Quellen

- Ich habe den Tipp mit dem wget Befehl um das Image runterzuladen von Max Müller bekommmen
