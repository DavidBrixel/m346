# Aufgabe 1.2

<!-- TOC -->

- [Aufgabe 1.2](#aufgabe-12)
  - [Q&A](#qa)
    - [Unterschied zwischen deklarativ und imperativ](#unterschied-zwischen-deklarativ-und-imperativ)
    - [Vorteile einer deklarativen Anwendung](#vorteile-einer-deklarativen-anwendung)
    - [Bedeutung von IaC](#bedeutung-von-iac)
    - [Vorteile von Iac mit Begründung](#vorteile-von-iac-mit-begr%C3%BCndung)
    - [Protokoll der REST-API Schnittstelle](#protokoll-der-rest-api-schnittstelle)
  - [Durchführung des Auftrages](#durchf%C3%BChrung-des-auftrages)
    - [Anforderungen](#anforderungen)
    - [Docker Compose File anpassen](#docker-compose-file-anpassen)
    - [app.py anpassen](#apppy-anpassen)
    - [Umgebung starten und wieder löschen](#umgebung-starten-und-wieder-l%C3%B6schen)

<!-- /TOC -->

## Q&A

### Unterschied zwischen deklarativ und imperativ

Imperativ steht das "wie" des Lösungsweges im Vordergrund. der Entwickler legt im Quellcode genau fest was der Computer Schritt für Schritt machen soll. die Imperative Variante konzentriert sich wie man Schritt für Schritt zur Lösung gelangt.

deklarativ wird das "was" der Lösung beschrieben. Die deklarative Variante konzentriert sich auf das Endergebnis.

### Vorteile einer deklarativen Anwendung

- komplexe Programme in einer komprimierter Form darzustellen
- vereinfachte Entwicklung: Fokusierung auf das "was" anstatt das "wie", müssen sich Entwickler weniger um die Implementierungsgründe kümmern
- bessere Lesbarkeit und Wartbarkeit: deklarative Anwendungen sind oft leicher zu lesen und zu verstehen, da sie sich auf die gewünschten Ergebnisse konzentrieren

### Bedeutung von IaC

IaC bedeutet so viel wie: Infrastructure as Code (eine deklaratives Manifest). Mit IaC werden Konfigurationsdateien erstellt, welche alle Infrastrukturspezifikationen enthalten.

### Vorteile von Iac mit Begründung

- geringe Kosten: mit der Automatisierung von der Verwaltung/Aufsetzung braucht man dafür weniger Zeit und kann damit Arbeitskräfte mühsame Arbeit sparen und spart somit Geld
- weniger Fehler: da die Konfiguration überschaubar in nur einem File gespeichert wird, passieren weniger schnell Fehler und erkennt sie dafür schneller
- Wiederverwendbarkeit: Sobald der Code für eine Infrastruktur geschrieben ist, lässt er sich jederzeit und so oft wie gewünscht ausführen

### Protokoll der REST-API Schnittstelle

die REST-API Schnittstelle verwendet HTTP-Anfragen, um per PUT, GET, POST und DELETE Informationen zu erstellen, zuzugreifen und zu löschen

## Durchführung des Auftrages

### Anforderungen

- Port der Webapp auf 5001
- Name des Volume heisst: Zaehler_Speicher
- Name des Netzwerk heisst: Internes_Netz
- Titel im Frontend heisst: ... um Dein Knowhow zu bestätigen ...

### Docker Compose File anpassen

```
version: "3.3"
services:
  web-fe:
    build: .
    command: python app.py
    ports:
      - target: 5000
        published: 5001
    networks:
      - Internes_Netz
    volumes:
      - type: volume
        source: Zaehler_Speicher
        target: /code
  redis:
    image: "redis:alpine"
    networks:
      Internes_Netz:

networks:
  Internes_Netz:

volumes:
  Zaehler_Speicher:
```

geändert wurden jeweils die **Zaehler_Speicher**, welcher vorher counter-vol hiessen. Ausserdem wurde der Name des Netzwerk angepasst welcher nun **Internes_Netz** heisst. Da in den Anforderungen der Port auf 5001 laufen sollte wird der Port bei `published: 5001` angepasst.

### app.py anpassen

Auf der Zeile 22 muss man **um Dein Skill-Level zu erhöhen** in **um Dein Knowhow zu bestätigen** ändern

### Umgebung starten und wieder löschen

```
#die gesamte Umgebung mit dem .yml File im Hintergrund starten
docker-compose up -d

#Umgebung löschen
docker-compose down

#alle Container sehen
docker ps -a

#images auflisten
docker image ls
```
