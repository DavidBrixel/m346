# Auftrag vom 23.06.23

<!-- TOC -->

- [Auftrag vom 23.06.23](#auftrag-vom-230623)
  - [Q&A](#qa)
    - [Vorteile von Podman gegenüber Docker?](#vorteile-von-podman-gegen%C3%BCber-docker)
    - [Was muss beim Tagging berücksichtigt werden, damit das OCI-Image später beim push nicht in's Default-Registry DockerHub kopiert wird?](#was-muss-beim-tagging-ber%C3%BCcksichtigt-werden-damit-das-oci-image-sp%C3%A4ter-beim-push-nicht-ins-default-registry-dockerhub-kopiert-wird)
    - [mit welchen command loggt man sich ins gitlab registry ein?](#mit-welchen-command-loggt-man-sich-ins-gitlab-registry-ein)
    - [mit welchen podman command sieht man alle image details?](#mit-welchen-podman-command-sieht-man-alle-image-details)
  - [Voraussetzungen](#voraussetzungen)
  - [Durchführung](#durchf%C3%BChrung)
    - [Podman Image erstellen und ins GitLab Repo hochladen.](#podman-image-erstellen-und-ins-gitlab-repo-hochladen)
    - [Ausführen des Podman Images](#ausf%C3%BChren-des-podman-images)
    - [Sichern des OCI-Images als Tar- oder Zip File](#sichern-des-oci-images-als-tar--oder-zip-file)

<!-- /TOC -->

## Q&A

### 3 Vorteile von Podman gegenüber Docker?

- Rootless-Betrieb: Podman ermöglicht die Ausführung von Containern ohne die Notwendigkeit eines separaten Daemon-Prozesses oder Root-Zugriffs. Dies erhöht die Sicherheit, da Container nicht mit erhöhten Privilegien laufen.

- Keine zusätzliche Netzwerkkommunikation: Im Gegensatz zu Docker führt Podman Aktionen wie das Verwalten von Containern und Images direkt aus, ohne zusätzliche Netzwerkkommunikation mit einem Daemon. Dies kann die Leistung verbessern und die Latenz reduzieren.

- Native Pod-Unterstützung: Podman bietet native Unterstützung für Pods, die eine Gruppe von Containern zusammenfassen. Dies erleichtert die Orchestrierung von Containern und ermöglicht eine bessere Zusammenarbeit zwischen ihnen, ohne auf externe Tools angewiesen zu sein.

### Was muss beim Tagging berücksichtigt werden, damit das OCI-Image später beim push nicht in's Default-Registry DockerHub kopiert wird?

Um zu verhindern, dass ein OCI-Image beim Pushen automatisch in die Standard-Registry DockerHub kopiert wird, sollte das Image vor dem Push mit dem entsprechenden Repository-Präfix getaggt werden. Das Tag sollte den vollständigen Pfad zur gewünschten Registry enthalten, zum Beispiel "registry.example.com/mein-image:latest". Dadurch wird das Image in die spezifizierte Registry hochgeladen.

### mit welchen command loggt man sich ins gitlab registry ein?

```
podman login registry.gitlab.com --username=david902
```

### mit welchen podman command sieht man alle image details?

```
podman image inspect <ID of Image>
podman image ls | grep -i <name of container>
```

## Voraussetzungen

- Wissen, welche Vorteile Microservices gegenüber monolithischen Anwendungen haben.
- Verstehen, was der Unterschied zwischen einer VM und einem OCI-Image ist.
- Die Unterschiede zwischen OCI-Images und Containern erklären können.
- Wissen, wie OCI-Images mit Podman erstellt werden
- In der Lage sein, OCI-Images in einem beliebigen Registry abzulegen.
- OCI-Images zu starten, zu stoppen und zu löschen.

## Durchführung

### Podman Image erstellen und ins GitLab Repo hochladen.

zuerst muss das repo geklont werden

```
git clone https://gitlab.com/ser-cal/oci-image-one.git
```

danach muss ein Image lokal erstellt werden

```
podman image build -t registry.gitlab.com/david902/m346/webapp-p8095_one:1.0 . #Beachte dass du dich im Ordner des Images befindest
```

mit diesem Command logt man sich ins Gitlab Registry ein

```
podman login registry.gitlab.com --username=david902
```

Wenn das Image gebuildet wurde kann man das Image auf Gitlab raufladen

```
podman image push registry.gitlab.com/david902/m346/webapp-p8095_one:1.0
```

### Ausführen des Podman Images

```
podman container run -d --name david902-web -p 8096:8095 registry.gitlab.com/david902/m346/webapp-p8095_one:1.0 #Starten und erstellen des Containers
podman container stop david902-web #Stoppen des Containers
podman container rm david902-web #Löschen des Containers
```

### Sichern des OCI-Images als Tar- oder Zip File

Nachdem das Image local vorhanden ist kann daraus ein Tar File oder Gzip erstellt werden

Erstellen des Files als tar

```
podman save registry.gitlab.com/david902/m346/webapp-p8095_one:1.0 --output webapp-p8095_one.tar
podman save <OCI-Image> --output <Filename>.tar
```

Erstellen des Files als gzip

```
podman save registry.gitlab.com/david902/m346/webapp-p8095_one:1.0 | gzip > webapp-p8095_one.tar.gz
podman save <OCI-Image> | gzip > <Filename>.tar.gz
```

entpacken des Files

```
podman load -i webapp-p8095_one.tar
podman load -i <Filename>.tar.gz <OCI-Image>
```
