# Dokumentation M346

<!-- TOC -->

- [Dokumentation M346](#dokumentation-m346)
  - [Vorwort](#vorwort)

<!-- /TOC -->

## Vorwort

In diesem Modul werden verschiedene Aufträge mit Docker, Docker Compose und Kubernetes gelöst (Container Orchestrierung). Daher wird für das Modul 346 ein gewisses Vorwissen zu container verlangt. Einfach Commands für das Vorwissen können in meiner Dokumentation zum Modul 169 gefunden werden.
